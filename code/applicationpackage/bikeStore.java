//Sriraam Nadarajah 2245165
package applicationpackage;

import vehiclespackage.bicycle;

public class bikeStore {
    public static void main (String [] args){
        bicycle[] bicycles = new bicycle[4];
        
        bicycles[0] = new bicycle("Specialized", 21 , 40.0);
        bicycles[1] = new bicycle("Sriraam", 20 , 42.0);
        bicycles[2] = new bicycle("Nadarajah", 19, 39.0);
        bicycles[3] = new bicycle("Java", 18, 29.0);

        for (int i =0; i < bicycles.length; i++){
            System.out.println(bicycles[i].toString());
        }

    }
}
