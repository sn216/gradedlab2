//Sriraam Nadarajah 2245165
package vehiclespackage;

public class bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;


    public bicycle (String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
    }

}
